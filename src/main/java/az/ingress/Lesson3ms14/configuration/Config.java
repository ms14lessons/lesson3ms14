package az.ingress.Lesson3ms14.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class Config {
    @Bean
    @Scope("singleton")
    public ModelMapper getModelMApper(){
        return new ModelMapper();
    }
}
