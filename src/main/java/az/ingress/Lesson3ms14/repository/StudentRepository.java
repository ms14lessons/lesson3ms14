package az.ingress.Lesson3ms14.repository;

import az.ingress.Lesson3ms14.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student,Integer> {

}
