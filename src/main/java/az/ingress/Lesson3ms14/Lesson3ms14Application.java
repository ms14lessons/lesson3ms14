package az.ingress.Lesson3ms14;

import az.ingress.Lesson3ms14.configuration.AppConfig;
import az.ingress.Lesson3ms14.model.Student;
import az.ingress.Lesson3ms14.service.TeacherService;
import az.ingress.Lesson3ms14.service.TeacherService2Impl;
import az.ingress.Lesson3ms14.service.TeacherServiceImpl;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class Lesson3ms14Application implements CommandLineRunner {
    /*  private final TeacherServiceImpl teacherService;
      private final TeacherServiceImpl teacherService2;
  */
 /*   @Value("${ms.name}")
    private String name;


    @Value("${ms.teacher}")
    private String teacher;
*/

    private final AppConfig appConfig;

    public static void main(String[] args) {
        SpringApplication.run(Lesson3ms14Application.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        /*
        System.out.println("Ms name is " + name);
        System.out.println("Ms teacher is " + teacher);
        */

        System.out.println("Ms name is " + appConfig.getName());
        System.out.println("Ms teacher is " + appConfig.getTeacher());
        System.out.println("Ms students are " + appConfig.getStudents());


    }


}
