package az.ingress.Lesson3ms14.model;

public final class SingletonClass {
    private static SingletonClass singletonClass;
    private SingletonClass() {

    }
    public static SingletonClass getInstance() {
        if (singletonClass == null) {
            synchronized (SingletonClass.class) {
                if (singletonClass == null) {
                    singletonClass = new SingletonClass();
                }
            }

        }
        return singletonClass;

    }
}