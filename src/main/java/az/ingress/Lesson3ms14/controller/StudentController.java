package az.ingress.Lesson3ms14.controller;

import az.ingress.Lesson3ms14.dto.CreateStudentDto;
import az.ingress.Lesson3ms14.dto.StudentDto;
import az.ingress.Lesson3ms14.dto.UpdateStudentDto;
import az.ingress.Lesson3ms14.model.Student;
import az.ingress.Lesson3ms14.service.StudentService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {
    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping
    public void create(@RequestBody CreateStudentDto student) {
        studentService.create(student);

    }

    @PutMapping
    public void update(@RequestBody UpdateStudentDto student) {
        studentService.update(student);

    }

    @GetMapping("/{id}")
    public StudentDto get(@PathVariable Integer id) {
        return studentService.get(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        studentService.delete(id);
    }


    @GetMapping
    public List<Student> getAll() {
        return studentService.getAll();
    }


}
