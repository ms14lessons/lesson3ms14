package az.ingress.Lesson3ms14.service;

import org.springframework.stereotype.Service;

@Service
public class A {
    private final B b;

    public A(B b) {
        this.b = b;
    }
}
