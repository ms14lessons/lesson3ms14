package az.ingress.Lesson3ms14.service;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
public class B {

    private final A a;

    public B(@Lazy A a) {
        this.a = a;
    }
}
