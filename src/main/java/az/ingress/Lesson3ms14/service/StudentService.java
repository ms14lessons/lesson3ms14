package az.ingress.Lesson3ms14.service;

import az.ingress.Lesson3ms14.dto.CreateStudentDto;
import az.ingress.Lesson3ms14.dto.StudentDto;
import az.ingress.Lesson3ms14.dto.UpdateStudentDto;
import az.ingress.Lesson3ms14.model.Student;
import az.ingress.Lesson3ms14.repository.StudentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
        this.modelMapper = new ModelMapper();
    }

    public void create(CreateStudentDto dto) {
//        var student = new Student();
//        student.setName(dto.getName());
//        student.setSurname(dto.getSurname());
//        student.setPassword(dto.getPassword());
        Student student = modelMapper.map(dto, Student.class);
        studentRepository.save(student);
    }

    public void update(UpdateStudentDto dto) {
        Optional<Student> entity = studentRepository.findById(dto.getId());
        entity.ifPresent(student1 -> {
            student1.setName(dto.getName() != null ? dto.getName() : student1.getName());
            student1.setSurname(dto.getSurname() != null ? dto.getSurname() : student1.getSurname());
            studentRepository.save(student1);
        });
    }

    public List<Student> getAll() {
        return studentRepository.findAll();
    }

    public StudentDto get(Integer id) {
        Student student = studentRepository.findById(id).get();
//      StudentDto studentDto = new StudentDto();
//        studentDto.setId(student.getId());
//        studentDto.setName(student.getName());
//        studentDto.setSurname(student.getSurname());
        StudentDto studentDto = modelMapper.map(student, StudentDto.class);
        return studentDto;
    }

    public void delete(Integer id) {
        studentRepository.deleteById(id);
    }
}
